import React, { Suspense } from "react";

import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";

import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import loadStore from "./redux/store";

import { createTheme } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: "#b99642",
    },
  },
});

let { store, persistor } = loadStore();

const Toolbar = React.lazy(() => import("./components/toolbar/index"));

function App() {
  return (
    <>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <ThemeProvider theme={theme}>
            <Suspense fallback={<div>Loading...</div>}>
              <Router>
                <Toolbar />
              </Router>
            </Suspense>
          </ThemeProvider>
        </PersistGate>
      </Provider>
    </>
  );
}

export default App;
