import { createSlice, configureStore } from "@reduxjs/toolkit";

const profileReducer = createSlice({
  name: "profile",
  initialState: {},
  reducers: {
    setProfile: (state,action) => ( action.payload ),
  },
});

export const { setProfile } = profileReducer.actions;
export default profileReducer.reducer;
