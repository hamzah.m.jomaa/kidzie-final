import {url} from "./config"
import axios from "axios"

export const Login = async ({ username, password }) => {
    const result = await axios.post(`${url}/user/login`, {
        username,
        password,
      })
      return result.data
}