import {url} from "./config"
import axios from "axios"



export const getOrdersData = async () =>{
    try{
        const returnedValues = await axios.get(url + "/orders")
        return returnedValues.data
    }catch(e){
        console.log(e)
    }
}

export const EditPromo = async({promo,id})=>{
    try{
        const returnedData = await axios.post(url+"/promo/edit/"+id,{promo})
        return returnedData.data
    }catch(err){
        console.log(err)
    }
}

export const getPromoCode = async(id)=>{
    try{
        const returnedData = await axios.get(url+"/promo/code/"+id)
        return returnedData.data
    }catch(err){
        console.log(err)
    }
}

export const stopPromoCode = async({id})=>{
    try{
        const returnedData = await axios.post(url+"/promo/stop",{id})
        return returnedData.data
    }catch(err){
        console.log(err)
    }
}

export const getPromoData = async () =>{
    try{
        const returnedValues = await axios.get(url + "/promo")
        return returnedValues.data.data
    }catch(e){
        console.log(e)
    }
}

export const AddPromo = async ({promo}) =>{
    try{
        const returnedData = await axios.post(url+"/promo/add",{promo})
        return returnedData.data
    }catch(err){
        console.log(err)
    }
}

export const RemoveSale = async ({saleIds}) =>{
    try{
        const returnedData = await axios.post(url+"/products/removesale",{saleIds})
        return returnedData.data
    }catch(err){
        console.log(err)
    }
}

export const AddSale = async ({saleIds,data}) =>{
    try{
        const returnedData = await axios.post(url+"/products/addsale",{saleIds,data})
        return returnedData.data
    }catch(err){
        console.log(err)
    }
}

export const Search = async ({search}) =>{
    try{
        const returnedData = await axios.post(url+"/products/search",{search})
        return returnedData.data
    }catch(err){
        console.log(err)
    }
}

export const GetSearchData = async () =>{
    try{
        const returnValue = await axios.get(url+"/products/search")
        return returnValue.data.data
    }catch(err){
        console.log(err)
    }
}

export const getProfile = async ({userId}) =>{
    try{
        const returnedData = await axios.post(url+"/profile",{userId})
        return returnedData.data
    }catch(err){
        console.log(err)
    }
}

export const getData = async () =>{
    try{
        const returnValue = await axios.get(url+"/register")
        return returnValue.data.data
    }catch(err){
        console.log(err)
    }
}
