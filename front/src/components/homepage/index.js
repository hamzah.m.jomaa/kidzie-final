import React from "react";
import Lottie from "react-lottie";
import animationData from "../../lottie/homepage.json";
import { Button, Typography } from "@material-ui/core";
import { useSelector } from "react-redux";
import styles from "./styles.module.scss";
import HomeImage from "../assets/Picture1.png";

export default function App() {
  const Profile = useSelector((state) => state.profile);
  console.log(Profile);
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <>
      <div className={styles.container}>
        <div className={styles.homepage}>
          <img src={HomeImage} />
          {/* <Lottie options={defaultOptions} height={400} width={400} /> */}
          <Typography>
            Once upon a time, it happened that a great passion for fashion alone
            with a unique know-how in the e-commerce industry and a long
            experience in technology added to a huge love for kids have all
            together driven two entrepreneurs to decide on launching <strong>kidzie</strong>. The
            whole story began in late 2017, whereas the dream came true in late
            2018 in Toronto, Canada.
            <br />
            Kidzei is an online retail for kids’ luxury fashion. It is neither
            the first in its kind, nor the last in its industry, yet Kidzie
            comes to outstand, lead and be different. Based on a revolutionary
            business modal, Kidzie forms a global digital marketplace that links
            worldwide customers with carefully curated products provided by a
            wide network of top-class independent boutiques from all across the
            world.
            <br />
            Kidzie guarantees an unparalleled customer experience ensured by a
            network of tech-savvy engineers, fashion experts, and marketing
            professionals whom are always there to maintain the best editional
            shopping experience possible.
            <br />
            Shopping at Kidzie is fun, easy, and advantageous. Through this
            platform, you make sure you are getting your kids dressed in the
            best. Whether you are looking to get your kids simple or crazy
            styles, occasional or frequent clothes, trendy or regular shoes, all
            you need is to navigate our platform and enjoy the rest. Customize
            your own cart, create your wishlist, choose your currency, and keep
            smiling
          </Typography>
        </div>
        {Profile.username ? (
          ""
        ) : (
          <div className={styles.btns}>
            <Button
              className={styles.LoginBtn}
              variant="contained"
              href="/login"
            >
              Login
            </Button>
            <Button
              className={styles.signupBtn}
              variant="contained"
              href="/register"
            >
              Signup
            </Button>
            </div>
        )}
      </div>
    </>
  );
}
