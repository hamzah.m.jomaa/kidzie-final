import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/";
import { getData, getProfile } from "../../api/Admin";
import {
  Button,
  Select,
  FormControl,
  FormControlLabel,
  MenuItem,
  Radio,
  RadioGroup,
  InputLabel,
  Input,
  Grid,
} from "@mui/material/";
import axios from "axios";
import { url } from "../../api/config";
import { useSelector } from "react-redux";
import styles from "./style.module.scss";

const CustomerData = () => {
  const [city, addCity] = useState();
  const [country, addCountry] = useState();
  const [state, setState] = useState();

  const handleEdit = () =>{
    alert()
  }

  return (
    <>
      <div style={{ marginTop: 50, width: "100%" }}>
        <Grid className={styles.container} container spacing={3}>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={styles.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                City
              </InputLabel>
              <Input
                onChange={({ target: { value } }) => addCity(value)}
                fullWidth
                value={city}
                placeholder="City"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={styles.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Country
              </InputLabel>
              <Input
                onChange={({ target: { value } }) => addCountry(value)}
                fullWidth
                value={country}
                placeholder="City"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={styles.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Province / State
              </InputLabel>
              <Input
                onChange={({ target: { value } }) => setState(value)}
                fullWidth
                value={state}
                placeholder="Province / State"
              />
            </FormControl>
          </Grid>
          <Grid item xs={6} sm={6}>
            <Button
              fullWidth
              className={styles.btnRegister}
              variant="contained"
              onClick={handleEdit}
            >
              Edit
            </Button>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default function FullWidthGrid() {
  const Profile = useSelector((state) => state.profile);
  const [nationality, setNationality] = useState("");
  const [UserId, setUserId] = useState("");
  const [firstName, setfirstName] = useState("");
  const [lastName, setlastName] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setphoneNumber] = useState("");
  const [country, setCountry] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [oldPassword, setOldPassword] = useState("");
  const [gender, setGender] = useState("");
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = useState(false);
  const [addressSection, setAddressSection] = useState(true);
  const [editMode, setEditMode] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const addUser = async (e) => {
    const finalPassword = password || oldPassword;
    let user = {
      firstName,
      lastName,
      email,
      phoneNumber,
      country,
      username,
      password: finalPassword,
      gender,
    };
    try {
      const UpdateUser = await axios.post(url + "/profile/update", {
        user,
        password,
        UserId,
      });
      if (UpdateUser.status === 200) {
        return alert("User Updated Successfully");
      }
      return alert("Error Occured");
      // window.location.href = "./"
    } catch (err) {
      alert(err);
    }
  };

  useEffect(async () => {
    setLoading(true);
    const userId = Profile.userId;
    Promise.all([getProfile({ userId }), getData()]).then((data) => {
      setUserId(userId);
      setUsername(data[0].user.username);
      setCountry(data[0].user.country);
      setfirstName(data[0].user.firstName);
      setlastName(data[0].user.lastName);
      setEmail(data[0].user.email);
      setGender(data[0].user.gender);
      setOldPassword(data[0].user.password);
      setphoneNumber(data[0].user.phoneNumber);
      setNationality(data[1].nationality);
      setLoading(false);
    });
  }, []);

  return (
    <>
      <div className={styles.root}>
        {loading ? (
          ""
        ) : (
          <>
            <Grid className={styles.container} container spacing={3}>
              <Grid item xs={12} sm={4}>
                <FormControl fullWidth className={styles.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    First Name
                  </InputLabel>
                  <Input
                    {...editMode}
                    onChange={({ target: { value } }) => setfirstName(value)}
                    fullWidth
                    value={firstName}
                    name="firstName"
                    placeholder="First Name"
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={4}>
                <FormControl fullWidth className={styles.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Last Name
                  </InputLabel>
                  <Input
                    onChange={({ target: { value } }) => setlastName(value)}
                    fullWidth
                    value={lastName}
                    name="lastName"
                    placeholder="Last Name"
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={4}>
                <FormControl component="fieldset">
                  <InputLabel id="demo-controlled-open-select-label"></InputLabel>
                  <RadioGroup
                    row
                    aria-label="position"
                    name="position"
                    defaultValue="top"
                  >
                    <FormControlLabel
                      value="female"
                      checked={gender === "female" ? "true" : ""}
                      control={<Radio color="primary" />}
                      label="Female"
                      labelPlacement="right"
                      onChange={({ target: { value } }) => setGender(value)}
                    />
                    <FormControlLabel
                      value="male"
                      checked={gender === "male" ? "true" : ""}
                      control={<Radio color="primary" />}
                      label="Male"
                      labelPlacement="right"
                      onChange={({ target: { value } }) => setGender(value)}
                    />
                  </RadioGroup>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={4}>
                <FormControl fullWidth className={styles.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Email Address
                  </InputLabel>
                  <Input
                    onChange={({ target: { value } }) => setEmail(value)}
                    fullWidth
                    value={email}
                    name="email"
                    placeholder="Email Address"
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={4}>
                <FormControl fullWidth className={styles.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Phone Number
                  </InputLabel>
                  <Input
                    onChange={({ target: { value } }) => setphoneNumber(value)}
                    fullWidth
                    value={phoneNumber}
                    name="phoneNumber"
                    placeholder="Phone Number"
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={4}>
                <FormControl fullWidth className={styles.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Nationality
                  </InputLabel>
                  <Select
                    fullWidth
                    labelId="demo-controlled-open-select-label"
                    id="demo-controlled-open-select"
                    open={open}
                    onClose={handleClose}
                    onOpen={handleOpen}
                    value={country}
                    variant="standard"
                    onChange={({ target: { value } }) => setCountry(value)}
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    {nationality
                      ? nationality.map((element) => {
                          return (
                            <MenuItem
                              key={element._id}
                              {...(element._id === country ? "selected" : "")}
                              value={element._id}
                            >
                              {element.title}
                            </MenuItem>
                          );
                        })
                      : ""}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl fullWidth className={styles.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Username
                  </InputLabel>
                  <Input
                    onChange={({ target: { value } }) => setUsername(value)}
                    fullWidth
                    name="username"
                    value={username}
                    placeholder="Username"
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl fullWidth className={styles.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Password
                  </InputLabel>
                  <Input
                    type="password"
                    onChange={({ target: { value } }) => setPassword(value)}
                    fullWidth
                    name="password"
                    placeholder="Password"
                  />
                </FormControl>
              </Grid>
              <div className={styles.btns}>
                <Grid items xs="3" sm="3">
                  {editMode ? (
                    <Button
                      fullWidth
                      className={styles.btnRegister}
                      variant="contained"
                      onClick={addUser}
                    >
                      Update
                    </Button>
                  ) : (
                    ""
                  )}
                </Grid>
                <Grid items xs="3" sm="3">
                  {Profile.role === "customer" ? (
                    <Button
                      fullWidth
                      className={styles.btnRegister}
                      variant="contained"
                      onClick={() => {
                        setAddressSection(!addressSection);
                      }}
                    >
                      Add Address
                    </Button>
                  ) : (
                    <Button
                      fullWidth
                      disabled
                      className={styles.btnRegister}
                      variant="contained"
                    >
                      Add Address
                    </Button>
                  )}
                </Grid>
                <Grid items xs="3" sm="3">
                  <Button
                    fullWidth
                    className={styles.btnRegister}
                    variant="contained"
                    onClick={() => {
                      setEditMode(!editMode);
                    }}
                  >
                    Edit
                  </Button>
                </Grid>
              </div>
            </Grid>
          </>
        )}
      </div>
      <div className={styles.root}>
        {!addressSection ? "" : <CustomerData />}
      </div>
    </>
  );
}
