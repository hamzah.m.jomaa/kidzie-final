import React, { useCallback, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {Button, FormControl,Input,Grid,InputLabel} from "@mui/material/";
import { Login } from "../../api/Auth";

import {useDispatch} from "react-redux"
import { setProfile } from "../../redux/reducers/profile";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 100,
    flexGrow: 1,
    display: "flex",
    justifyContent: "center",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  button: {
    display: "block",
    marginTop: theme.spacing(2),
  },
  formControl: {
    minWidth: 120,
  },
  container: {
    borderColor: "#b99642",
    borderStyle: "solid",
    borderWidth: 2,
    padding: 20,
    boxShadow: "10px 6px 29px -13px rgba(0,0,0,0.75)",
  },
  btnLogin: {
    marginTop: 10,
  },
}));

export default function FullWidthGrid() {
  const dispatch = useDispatch()

  const [username, setUsername] = useState([]);
  const [password, setPassword] = useState([]);
  const [error, setError] = useState([""]);

  const classes = useStyles();

  const login = useCallback(async () => {
    try {
      
      setError(null);

      const res = await Login({ username, password });
      
      if (res.status === 200) {
        dispatch(setProfile(res.data))
        window.location.href = "./"
      } else {
        setError(res.message);
      }
    } catch (err) {
      setError("حدث خطأ ما يرجى إعادة المحاولة");
      console.log(err);
    }
  });

  return (
    <div className={classes.root}>
      <form>
        <Grid className={classes.container} container spacing={1}>
          <Grid item xs={12} sm={12}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Username
              </InputLabel>
              <Input
                onChange={({ target: { value } }) => setUsername(value)}
                fullWidth
                name="username"
                placeholder="Username"
                color="primary"
                variant="standard"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={12}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Password
              </InputLabel>
              <Input
                type="password"
                onChange={({ target: { value } }) => setPassword(value)}
                fullWidth
                name="password"
                placeholder="Password"
              />
            </FormControl>
          </Grid>
          {error}
          <Grid items xs="12" sm="12" style={{margin: "18px 18px 0"}} >
            <Button
              className={classes.btnLogin}
              fullWidth
              variant="contained"
              color="primary"
              onClick={login}
            >
              Login
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
}
