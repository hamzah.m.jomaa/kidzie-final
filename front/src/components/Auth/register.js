import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Input from "@material-ui/core/Input";
import { getData } from "../../api/Admin";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import axios from "axios";
import { url } from "../../api/config";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import { Typography } from "@mui/material";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "center",
    margin: "5%",
  },
  container: {
    borderColor: "#b99642",
    borderStyle: "solid",
    borderWidth: 2,
    padding: 10,
    boxShadow: "10px 6px 29px -13px rgba(0,0,0,0.75)",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  button: {
    display: "block",
    marginTop: theme.spacing(2),
  },
  formControl: {
    minWidth: 120,
  },
  btnRegister: {
    marginTop: 10,
  },
}));

export default function FullWidthGrid() {
  const [nationality, setNationality] = useState([]);

  const [firstName, setfirstName] = useState([]);
  const [lastName, setlastName] = useState([]);
  const [email, setEmail] = useState([]);
  const [phoneNumber, setphoneNumber] = useState([]);
  const [country, setCountry] = useState([]);
  const [username, setUsername] = useState([]);
  const [password, setPassword] = useState([]);
  const [gender, setGender] = useState([]);
  const [role,setRole] = useState([])

  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [openRole, setOpenRole] = React.useState(false);


  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleCloseRole = () => {
    setOpenRole(false);
  };

  const handleOpenRole = () => {
    setOpenRole(true);
  };

  const addUser = async (e) => {
    let user = {
      firstName,
      lastName,
      email,
      phoneNumber,
      country,
      username,
      password,
      gender,
      role
    };
    try {
      const UserAdded = await axios.post(url + "/user/register", user);
      alert("User Added Successfully");
      localStorage.setItem("user", JSON.stringify(UserAdded.data));
      window.location.href = "./";
    } catch (err) {
      alert(err);
    }
  };

  useEffect(async () => {
    const nationalitybackend = await getData();
    setNationality(nationalitybackend.nationality);
  }, []);

  return (
    <div className={classes.root}>
      <form onSubmit={addUser}>
        <Grid className={classes.container} container spacing={3}>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                First Name
              </InputLabel>
              <Input
                onChange={({ target: { value } }) => setfirstName(value)}
                fullWidth
                name="firstName"
                placeholder="First Name"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Last Name
              </InputLabel>
              <Input
                onChange={({ target: { value } }) => setlastName(value)}
                fullWidth
                name="lastName"
                placeholder="Last Name"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl component="fieldset">
              <InputLabel id="demo-controlled-open-select-label"></InputLabel>
              <RadioGroup
                row
                aria-label="position"
                name="position"
                defaultValue="top"
              >
                <FormControlLabel
                  value="female"
                  control={<Radio color="primary" />}
                  label="Female"
                  labelPlacement="right"
                  onChange={({ target: { value } }) => setGender(value)}
                />
                <FormControlLabel
                  value="male"
                  control={<Radio color="primary" />}
                  label="Male"
                  labelPlacement="right"
                  onChange={({ target: { value } }) => setGender(value)}
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Email Address
              </InputLabel>
              <Input
                onChange={({ target: { value } }) => setEmail(value)}
                fullWidth
                name="email"
                placeholder="Email Address"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Phone Number
              </InputLabel>
              <Input
                onChange={({ target: { value } }) => setphoneNumber(value)}
                fullWidth
                name="phoneNumber"
                placeholder="Phone Number"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Nationality
              </InputLabel>
              <Select
                fullWidth
                labelId="demo-controlled-open-select-label"
                id="demo-controlled-open-select"
                open={open}
                onClose={handleClose}
                onOpen={handleOpen}
                value={country}
                onChange={({ target: { value } }) => setCountry(value)}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {nationality
                  ? nationality.map((element) => {
                      return (
                        <MenuItem key={element._id} value={element._id}>
                          {element.title}
                        </MenuItem>
                      );
                    })
                  : ""}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Username
              </InputLabel>
              <Input
                onChange={({ target: { value } }) => setUsername(value)}
                fullWidth
                name="username"
                placeholder="Username"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                Password
              </InputLabel>
              <Input
                type="password"
                onChange={({ target: { value } }) => setPassword(value)}
                fullWidth
                name="password"
                placeholder="Password"
              />
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl fullWidth className={classes.formControl}>
              <InputLabel id="demo-controlled-open-select-label">
                User Role
              </InputLabel>
              <Select
                fullWidth
                labelId="demo-controlled-open-select-label"
                id="demo-controlled-open-select"
                open={openRole}
                onClose={handleCloseRole}
                onOpen={handleOpenRole}
                value={role}
                onChange={({ target: { value } }) => setRole(value)}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="admin">Admin</MenuItem>
                <MenuItem value="partner">Partner</MenuItem>
                <MenuItem value="customer">Customer</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid items xs="12" sm="12">
              <Typography>
              By clicking register, you agree to our <strong>Terms & Conditions</strong> and <strong>Privacy Policy</strong>
              </Typography>
          </Grid>
          <Grid items xs="12" sm="12">
            <Button
              fullWidth
              className={classes.btnRegister}
              variant="contained"
              type="submit"
            >
              Register
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
}
