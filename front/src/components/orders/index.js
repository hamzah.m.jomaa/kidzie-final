import React, { useEffect, useState } from "react";
import { getOrdersData } from "../../api/Admin";
import Table from "./table"

function Orders() {
  let [orders, SetOrders] = useState([]);
  useEffect(async () => {
    try {
      let data = await getOrdersData();
      SetOrders(data);
    } catch (e) {}
  }, []);

  return (
    <>
      <h1>Hello World</h1>
      <Table orders={orders}/>
    </>
  );
}

export default Orders;
