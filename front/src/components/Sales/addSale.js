import React, { useEffect, useState } from "react";
import {
  Grid,
  FormControl,
  InputLabel,
  Input,
  Select,
  MenuItem,
  Typography,
  TextField,
  Button,
} from "@material-ui/core";
import styles from "./sales.module.scss";

function AddSale(props) {
  const [fromDate, setFromDate] = useState();
  const [toDate, setToDate] = useState();
  const [sale, setSale] = useState();
  const handleDateFromChange = (event) => {
    setFromDate(event.target.value);
  };

  const handleDateToChange = (event) => {
    setToDate(event.target.value);
  };

  const submitSale = () =>{
    let saleData = {
      saleFrom: fromDate,
      saleTo: toDate,
      saleAmount: sale
    }
    props.onSaleSubmit(saleData)
  }


  return (
    <>
      <FormControl fullWidth className={styles.date}>
        <Grid item xs={2} sm={2}>
          <Typography className={styles.title}>Add Sale</Typography>
        </Grid>
        <Grid item xs={4} sm={4}>
          <TextField
            id="from"
            label="From"
            type="date"
            value={fromDate}
            onChange={handleDateFromChange}
            sx={{ width: 220 }}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item xs={4} sm={4}>
          <TextField
            id="to"
            label="To"
            type="date"
            value={toDate}
            onChange={handleDateToChange}
            sx={{ width: 220 }}
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              min: fromDate,
            }}
          />
        </Grid>
        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-controlled-open-select-label">
              Kidzie Id
            </InputLabel>
            <Input
              onChange={({ target: { value } }) => setSale(value)}
              fullWidth
              value={sale}
              name="sale"
              placeholder="Sale"
            />
          </FormControl>
        </Grid>
        <Button onClick={submitSale} className={styles.btnSearch} fullWidth variant="contained">Add Sale</Button>

      </FormControl>
    </>
  );
}

export default AddSale;
