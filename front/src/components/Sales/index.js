import React, { useEffect, useState } from "react";
import Form from "./form";
import Table from "./table";
import { GetSearchData,AddSale,RemoveSale } from "../../api/Admin";
import AddSaleComponent from "./addSale"
import styles from "./sales.module.scss"
import {Button,Grid} from "@material-ui/core"

function Sales() {
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState("")
  const [addSale,setAddSale] = useState(true);
  const [saleIds,setSaleIds] = useState([])

  const FetchResults = (result) =>{
    setFilteredProducts(result)
  }

  const toggleAddSale = () =>{
    setAddSale(!addSale)
  }

  const fetchSelected = async (data) =>{
    setSaleIds(data)
  }

  const removeSale = async (data) =>{
    if(saleIds.length === 0){
      return alert("Choose Items!")
    }

    const result = await RemoveSale({saleIds})
    if (result.statusCode === 200){
      return alert(result.statusMessage)
    }

  }

  const SubmitSale = async (data) =>{
    if(saleIds.length === 0){
      return alert("Choose Items!")
    }
    if(!data.saleAmount){
      return alert("Choose Sales Specifications")
    }

    const result = await AddSale({saleIds,data})
    if (result.statusCode === 200){
      return alert(result.statusMessage)
    }
  }

  useEffect(async () => {
    if(filteredProducts){
      setProducts(filteredProducts)
    }else{
      const productsData = await GetSearchData();
    let s = 0
    productsData.products.map(element=>{
      return element.id = s++
    })
    setProducts(productsData.products);
    }
    
  }, [toggleAddSale,RemoveSale]);
  
  return (
    <>
      <Form onSearchResult={FetchResults} />
      <div className={styles.table}>
        <Table getSelected={fetchSelected} products={products} />
      </div>

      <Grid container class={styles.actions}>
        <Grid className={styles.AddSale} item xs={8} sm={8}>
          {addSale?<AddSaleComponent onSaleSubmit={SubmitSale} />:""}
        </Grid>
        <Grid className={styles.btnAdd} item xs={1} sm={1}>
          <Button onClick={toggleAddSale} className={styles.btnSearch} fullWidth variant="contained">Add Sale</Button>
        </Grid>
        <Grid className={styles.btnAdd} item xs={2} sm={2}>
          <Button onClick={removeSale} className={styles.btnSearch} fullWidth variant="contained">Remove Sale</Button>
        </Grid>
      </Grid>
    </>
  );
}

export default Sales;
