import React, { useState } from "react";
import {
  Grid,
  FormControl,
  InputLabel,
  Input,
  Select,
  MenuItem,
  Typography,
  TextField,
  Button,
} from "@material-ui/core";
import SelectNew from "react-select";

import styles from "./sales.module.scss";
import { Search } from "../../api/Admin";

const names = [
  {value:"polos",label:"Polos"},
  {value:"t-shirts",label:"T-Shirts"},
  {value:"graphics",label:"Graphics"},
  {value:"blouses",label:"Blouses"}
];
const saleNumber = [];
for (let i = 5; i <= 80; i += 5) {
  saleNumber.push(i);
}



function SalesForm(props) {
  const [id, setId] = useState("");
  const [productGroup, setproductGroup] = useState("");
  const [categories, setCategories] = useState([]);
  const [gender, setGender] = useState("");
  const [onSale, setOnSale] = useState();
  const [toDate, setToDate] = useState("");
  const [fromDate, setFromDate] = useState("");
  const [sale, setSale] = useState("");
  const [age, setAge] = useState("");
  const [designer, setDesigner] = useState("");

  const handleAgeChange = (event) => {
    setAge(event.target.value);
  };

  const handleChangeProductGroup = (event) => {
    setproductGroup(event.target.value);
  };

  const handleGenderChange = (event) => {
    setGender(event.target.value);
  };

  const handleSaleNumbeChange = (event) => {
    setSale(event.target.value);
  };

  const handleSaleChange = (event) => {
    setOnSale(event.target.value);
  };

  const handleCatgoriesChange = (event) => {
    const { options } = event.target;
    const value = [];
    for (let i = 0, l = options.length; i < l; i += 1) {
      if (options[i].selected) {
        value.push(options[i].value);
      }
    }
    setCategories(value);
  };

  const handleDateToChange = (event) => {
    setToDate(event.target.value);
  };

  const handleDateFromChange = (event) => {
    setFromDate(event.target.value);
  };

  const handleDesignerChange = (event) => {
    setDesigner(event.target.value);
  };

  const handleClickSearch = async () => {
    const search = {
      id,
      date: {
        fromDate,
        toDate,
      },
      group: productGroup,
      onSale,
      age,
      sale,
      gender,
      designer,
      categories,
    };

    console.log(search);
    setAge("");
    setCategories([]);
    setDesigner("");
    setFromDate("");
    setToDate("");
    setproductGroup("");
    setGender("");
    setOnSale("");
    const searchResponse = await Search({ search });
    props.onSearchResult(searchResponse);
  };

  return (
    <>
      <Grid container className={styles.form}>
        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-controlled-open-select-label">
              Kidzie Id
            </InputLabel>
            <Input
              onChange={({ target: { value } }) => setId(value)}
              fullWidth
              value={id}
              name="kidzieId"
              placeholder="kidzieId"
            />
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth className={styles.date}>
            <Typography className={styles.title}>Products Added</Typography>
            <Grid xs={5} sm={5}>
              <TextField
                id="from"
                label="From"
                type="date"
                value={fromDate}
                onChange={handleDateFromChange}
                sx={{ width: 220 }}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid xs={5} sm={5}>
              <TextField
                id="to"
                label="To"
                type="date"
                value={toDate}
                onChange={handleDateToChange}
                sx={{ width: 220 }}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  min: fromDate,
                }}
              />
            </Grid>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Product Group</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              value={productGroup}
              label="Product Group"
              onChange={handleChangeProductGroup}
            >
              <MenuItem value={1}>Group One</MenuItem>
              <MenuItem value={2}>Group Two</MenuItem>
              <MenuItem value={3}>Group Three</MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">On Sale</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              value={onSale}
              label="Product Group"
              onChange={handleSaleChange}
            >
              <MenuItem value={true}>Yes</MenuItem>
              <MenuItem value={false}>No</MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Age Group</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              value={age}
              label="Product Group"
              onChange={handleAgeChange}
            >
              <MenuItem value={10}>0 - 10</MenuItem>
              <MenuItem value={20}>10 - 20</MenuItem>
              <MenuItem value={30}>20 - 30</MenuItem>
              <MenuItem value={40}>30 - 40</MenuItem>
              <MenuItem value={40}>40 - 50</MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Sale</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              value={sale}
              label="Sale"
              onChange={handleSaleNumbeChange}
            >
              {saleNumber.map((value) => {
                return <MenuItem value={value}>{value}%</MenuItem>;
              })}
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Gender</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              value={gender}
              label="Gender"
              onChange={handleGenderChange}
            >
              <MenuItem value="Female">Female</MenuItem>
              <MenuItem value="Male">Male</MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Designer</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              value={designer}
              label="Product Group"
              onChange={handleDesignerChange}
            >
              <MenuItem value={1}>Bahaa Sayegh</MenuItem>
              <MenuItem value={2}>Amer AlKrad</MenuItem>
              <MenuItem value={3}>Hamzah Jomaa</MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <SelectNew className={styles.selectCategories} isMulti options={names} />
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <Button
            onClick={handleClickSearch}
            className={styles.btnSearch}
            fullWidth
            variant="contained"
          >
            Search
          </Button>
        </Grid>
      </Grid>
    </>
  );
}

export default SalesForm;
