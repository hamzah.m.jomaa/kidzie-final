import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { Grid } from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 500,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const designers = ["Bahaa Sayegh", "Amer AlKrad", "Hamzah Jomaa"];

const Groups = ["Group One", "Group Two", "Group Three"];

export default function BasicModal(props) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Button onClick={handleOpen}>Criteria</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid container style={{ width: "100%", textAlign: "center" }}>
            <Grid item xs={4}>
              <Typography style={{ fontWeight: "bolder" }} sx={{ mt: 2 }}>
                Categories
              </Typography>
              {props.criteria.categories.map((element) => {
                return (
                  <>
                    <Typography sx={{ mt: 2 }}>{element}</Typography>
                  </>
                );
              })}
            </Grid>
            <Grid item xs={4}>
              <Typography style={{ fontWeight: "bolder" }} sx={{ mt: 2 }}>
                Designer
              </Typography>
              <Typography sx={{ mt: 2 }}>
                {designers[props.criteria.productGroup - 1]}
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography style={{ fontWeight: "bolder" }} sx={{ mt: 2 }}>
                Product Group
              </Typography>
              <Typography sx={{ mt: 2 }}>
                {Groups[props.criteria.productGroup - 1]}
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </div>
  );
}
