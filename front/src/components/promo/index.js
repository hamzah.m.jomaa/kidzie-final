import React, { useEffect, useState } from "react";
import PromoForm from "./form";
import PromoTable from "./table";
import { getPromoData } from "../../api/Admin";
import styles from "./style.module.scss";
import { Button } from "@material-ui/core";

function Promo() {
  const [data, setData] = useState();
  const [editableCode, setEditableCode] = useState(false);
  const [promoSection,setPromoSection] = useState(false)
  const [editMode,setEditMode] = useState(false)

  const fetchPromoCode = (data) => {
    setEditMode(true)
    setPromoSection(true)
    setEditableCode(data.promo);
  };

  useEffect(async () => {
    const returnedData = await getPromoData();
    setData(returnedData);
  }, [editableCode]);

  return (
    <>
      <div>
        {data ? (
          <PromoTable onGetPromoCode={fetchPromoCode} promo={data.promo} />
        ) : (
          ""
        )}
        <Button onClick={()=>{setPromoSection(!promoSection)}} className={styles.addPromo} fullWidth variant="contained">
           Promo Section
        </Button>
      </div>
      {data && promoSection ? (
        <PromoForm
          promo={editableCode ? editableCode : false}
          products={data.products}
          editMode={editMode}
        />
      ) : (
        ""
      )}
    </>
  );
}
export default Promo;
