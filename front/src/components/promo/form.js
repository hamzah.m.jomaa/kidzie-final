import React, { useEffect, useState } from "react";
import {
  Grid,
  FormControl,
  InputLabel,
  Input,
  Select,
  MenuItem,
  Typography,
  TextField,
  Button,
} from "@material-ui/core";
import SelectNew from "react-select";

import { EditPromo, AddPromo } from "../../api/Admin";

import styles from "./style.module.scss";

const names = [
  { value: "polos", label: "Polos" },
  { value: "t-shirts", label: "T-Shirts" },
  { value: "graphics", label: "Graphics" },
  { value: "blouses", label: "Blouses" },
];

const saleNumber = [];
for (let i = 5; i <= 20; i += 5) {
  saleNumber.push(i);
}

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function prettyDate(date) {
  if (date) {
    const month = parseInt(new Date(date).getMonth()) + 1;
    const day = String(new Date(date).getDate()).padStart(2, "0");
    const year = new Date(date).getFullYear();
    return year + "-" + String(month).padStart(2, "0") + "-" + day;
  }
  return "";
}

function Promo(props) {
  let promo = useState(makeid(5));
  const [productGroup, setproductGroup] = useState("");
  const [toDate, setToDate] = useState("");
  const [fromDate, setFromDate] = useState("");
  const [promoPercent, setPromoPercnet] = useState("");
  const [designer, setDesigner] = useState("");
  const [productId, setProductsId] = useState([]);
  const [promoCodeForm, setPromoCodeForm] = promo;
  const [promoCode, setPromoCode] = promo;
  const [email, setEmail] = useState([]);
  const [categories, selectCategories] = useState([]);

  const handleChangeProductGroup = (event) => {
    setproductGroup(event.target.value);
  };

  const handlePromoPercnet = (event) => {
    props.promp ? props.promo.promoPercent = event.target.value : setPromoPercnet(event.target.value);
    
  };

  const handleDateToChange = (event) => {
    props.promp ? props.promo.toDate = event.target.value : setToDate(event.target.value);
  };

  const handleDateFromChange = (event) => {
    props.promo ? props.promo.FromDate = event.target.value:setFromDate(event.target.value);
  };

  const handleDesignerChange = (event) => {
    setDesigner(event.target.value);
  };

  const handleProductsId = (event) => {
    let productIds = [];
    event.map((element) => {
      productIds.push(element.value);
    });
    props.promo? props.promo.productId = event.target.value:    setProductsId(productIds);

  };

  const handleAddPromo = async () => {
    let promo = {
      promoStatus: true,
      productId,
      promoPercent,
      promoCode: promoCodeForm,
      fromDate,
      toDate,
      email,
      promoCiteria: {
        categories,
        productGroup,
        designer,
      },
    };
    const AddedPromo = await AddPromo({ promo });
    window.location.reload();
  };

  const handleEditPromo = async () => {
    let promo = {
      promoStatus: true,
      productId,
      promoPercent,
      promoCode: promoCodeForm,
      fromDate,
      toDate,
      email,
    };

    const promodEdited = await EditPromo({ promo, id: props.promo._id });
  };

  const handleCategoryChange = (event) => {
    let categoriesSelected = [];
    event.map((element) => {
      categoriesSelected.push(element.value);
    });
    selectCategories(categoriesSelected);
  };

  return (
    <>
      <Grid container className={styles.form}>
        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <SelectNew
              placeholder="Promo Code on"
              className={styles.selectCategories}
              onChange={handleProductsId}
              isMulti
              options={props.products}
            />
          </FormControl>
        </Grid>
        <Grid item xs={5} sm={5}>
          <FormControl fullWidth className={styles.date}>
            <Typography className={styles.title}>Promo</Typography>
            <Grid xs={5} sm={5}>
              <TextField
                id="from"
                label="From"
                type="date"
                value={
                  props.promo ? prettyDate(props.promo.fromDate) : fromDate
                }
                onChange={handleDateFromChange}
                sx={{ width: 220 }}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
            <Grid item xs={5} sm={5}>
              <TextField
                id="to"
                label="To"
                type="date"
                value={props.promo ? prettyDate(props.promo.toDate) : toDate}
                onChange={handleDateToChange}
                sx={{ width: 220 }}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  min: fromDate,
                }}
              />
            </Grid>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Designer</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              value={props.promo ? props.promo.promoCiteria.designer : designer}
              label="Product Group"
              onChange={handleDesignerChange}
            >
              <MenuItem value={1}>Bahaa Sayegh</MenuItem>
              <MenuItem value={2}>Amer AlKrad</MenuItem>
              <MenuItem value={3}>Hamzah Jomaa</MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Promo %</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              value={props.promo ? props.promo.promoPercent : promoPercent}
              label="Sale"
              onChange={handlePromoPercnet}
            >
              {saleNumber.map((value) => {
                return <MenuItem value={value}>{value}%</MenuItem>;
              })}
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Product Group</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              value={
                props.promo
                  ? props.promo.promoCiteria.productGroup
                  : productGroup
              }
              label="Product Group"
              onChange={handleChangeProductGroup}
            >
              <MenuItem value={1}>Group One</MenuItem>
              <MenuItem value={2}>Group Two</MenuItem>
              <MenuItem value={3}>Group Three</MenuItem>
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-controlled-open-select-label">
              Customer Emails
            </InputLabel>
            <Input
              onChange={({ target: { value } }) => setEmail(value.split(","))}
              fullWidth
              multiline
              value={props.promo ? props.promo.email : email}
              name="kidzieId"
              placeholder="kidzieId"
            />
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <SelectNew
              placeholder="Categories"
              className={styles.selectCategories}
              onChange={handleCategoryChange}
              isMulti
              options={names}
            />
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <FormControl fullWidth>
            <InputLabel id="demo-controlled-open-select-label">
              Kidzie Id
            </InputLabel>
            <Input
              onChange={({ target: { value } }) => setPromoCodeForm(value)}
              fullWidth
              value={props.promo ? props.promo.promoCode : promoCode}
              name="promo"
              placeholder="promo"
            />
          </FormControl>
        </Grid>

        <Grid item xs={5} sm={5}>
          <Button
            onClick={props.editMode ? handleEditPromo : handleAddPromo}
            className={styles.btnSearch}
            fullWidth
            variant="contained"
          >
            Add Promo Code
          </Button>
        </Grid>
      </Grid>
    </>
  );
}

export default Promo;
