import * as React from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import Popover from "@mui/material/Popover";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import { visuallyHidden } from "@mui/utils";
import { Button } from "@material-ui/core";
import { stopPromoCode,getPromoCode } from "../../api/Admin";
import EmailModal from "./emailModal";
import CriteriaModal from "./criteriaModal";

const prettyDate = (date) => {
  if (date) {
    const DATE = new Date(date);
    return DATE.toDateString();
  }
  return "";
};

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "promoCode",
    numeric: false,
    disablePadding: true,
    label: "Code",
  },
  {
    id: "promoStatus",
    numeric: false,
    disablePadding: true,
    label: "Status",
  },
  {
    id: "fromDate",
    numeric: false,
    disablePadding: true,
    label: "Start",
  },
  {
    id: "toDate",
    numeric: false,
    disablePadding: true,
    label: "Ends",
  },
  {
    id: "promoPercent",
    numeric: false,
    disablePadding: true,
    label: "Percentage",
  },
  {
    id: "emails",
    numeric: false,
    disablePadding: true,
    label: "Percentage",
  },
  {
    id: "promoCiteria",
    numeric: false,
    disablePadding: false,
    label: "Criteria",
  },
  {
    id: "actions",
    numeric: false,
    disablePadding: false,
    label: "Actions",
  },
];

function EnhancedTableHead(props) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align="center"
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const EnhancedTableToolbar = (props) => {
  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
      }}
    >
      <Typography
        sx={{ flex: "1 1 100%" }}
        variant="h6"
        id="tableTitle"
        component="div"
      >
        Promo Codes
      </Typography>
    </Toolbar>
  );
};

export default function EnhancedTable(props) {
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("calories");
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleStop = async (id) => {
    const message = await stopPromoCode({ id });
    alert(message.statusMessage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleEdit = async (id) =>{
    const promo_code = await getPromoCode(id)
    props.onGetPromoCode(promo_code.data)
  }




  // Avoid a layout jump when reaching the last page with empty props.promo.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - props.promo.length) : 0;

  return (
    <Box sx={{ width: "100%" }}>
      <Paper sx={{ width: "100%", mb: 2 }}>
        <EnhancedTableToolbar />
        <TableContainer>
          <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle">
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={props.promo.length}
            />
            <TableBody>
              {/* if you don't need to support IE11, you can replace the `stableSort` call with:
                 props.promo.slice().sort(getComparator(order, orderBy)) */}
              {stableSort(props.promo, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.name}
                    >
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                      >
                        {row.promoCode}
                      </TableCell>
                      <TableCell align="center">
                        {row.promoStatus ? "Active" : "INACTIVE"}
                      </TableCell>
                      <TableCell align="center">
                        {row.promoStatus ? prettyDate(row.fromDate) : ""}
                      </TableCell>
                      <TableCell align="center">
                        {row.promoStatus ? prettyDate(row.toDate) : ""}
                      </TableCell>
                      <TableCell align="center">
                        {row.promoStatus ? row.promoPercent + "%" : ""}
                      </TableCell>
                      <TableCell align="center">
                        {row.promoStatus ? (
                          <>
                            <EmailModal emails={row.email} />
                          </>
                        ) : (
                          ""
                        )}
                      </TableCell>
                      <TableCell align="center">
                        {row.promoStatus ? (
                          <>
                            <CriteriaModal criteria={row.promoCiteria} />
                          </>
                        ) : (
                          ""
                        )}{" "}
                      </TableCell>
                      <TableCell align="center">
                        <Button onClick={async () => handleEdit(row._id)}>
                          Edit
                        </Button>
                        <Button onClick={async () => handleStop(row._id)}>
                          Stop
                        </Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.promo.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
  );
}
