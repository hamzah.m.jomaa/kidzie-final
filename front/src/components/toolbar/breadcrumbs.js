import React from 'react';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

function handleClick(event) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}
    
export default function SimpleBreadcrumbs() {
  const currentURL = window.location.href // returns the absolute URL of a page

const pathname = window.location.pathname
  return (
    <Breadcrumbs aria-label="breadcrumb">
      <Link color="inherit" href="/" onClick={handleClick}>
        Kidize Homepage
      </Link>
      <Typography color="textPrimary">{pathname.split("/")[1]}</Typography>
    </Breadcrumbs>
  );
}