import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Logo from "./asset/kidzie_logo.jpg";
import BreadCrumbs from "./breadcrumbs";
import Login from "../Auth/login";
import Homepage from "../homepage/index";
import Register from "../Auth/register";
import Profile from "../profile/profile";
import styles from "./asset/styles.module.scss";
import { useSelector } from "react-redux";
import { Route, Link, Switch, Redirect } from "react-router-dom";
import {
  Home,
  Person,
  LibraryBooks,
  MonetizationOn,
  Receipt,
  Forward5,
  FindInPage,
  ReceiptLong,
  Logout,
  Loyalty,
} from "@mui/icons-material";

import Sales from "../Sales";
import Promo from "../promo";
import Orders from "../orders";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: "#FFFFFF",
    boxShadow: 0,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    backgroundColor: "#b99642",
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#b99642",
    color: "#fff",
  },
  drawerContainer: {
    overflow: "auto",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  logo: {
    width: 130,
  },
}));

export default function ClippedDrawer() {
  const profile = useSelector((state) => state.profile);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <img className={classes.logo} src={Logo} />
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <List>
            <Link to="home" className={styles.link}>
              <ListItem button key={"Home"}>
                <ListItemIcon>
                  <Home />
                </ListItemIcon>
                <ListItemText primary={"Home"} />
              </ListItem>
            </Link>
            <Link to="profile" className={styles.link}>
              <ListItem button key={"Profile"}>
                <ListItemIcon>
                  <Person />
                </ListItemIcon>
                <ListItemText primary={"Profile"} />
              </ListItem>
            </Link>
            {profile.role == "customer" ? (
              ""
            ) : (
              <Link to="kidiziecatalouge" className={styles.link}>
                <ListItem button key={"Kidizie Catalouge"}>
                  <ListItemIcon>
                    <LibraryBooks />
                  </ListItemIcon>
                  <ListItemText primary={"Kidizie Catalouge"} />
                </ListItem>
              </Link>
            )}

            {profile.role == "admin" ? (
              ""
            ) : (
              <Link to="inventory_control" className={styles.link}>
                <ListItem button key={"Inventory Control"}>
                  <ListItemIcon>
                    <LibraryBooks />
                  </ListItemIcon>
                  <ListItemText primary={"Inventory Control"} />
                </ListItem>
              </Link>
            )}

            {profile.role == "customer" ? (
              ""
            ) : (
              <Link to="sales" className={styles.link}>
                <ListItem button key={"Sales"}>
                  <ListItemIcon>
                    <MonetizationOn />
                  </ListItemIcon>
                  <ListItemText primary={"Sales"} />
                </ListItem>
              </Link>
            )}
            <Link to="orders" className={styles.link}>
              <ListItem button key={"Orders"}>
                <ListItemIcon>
                  <Receipt />
                </ListItemIcon>
                <ListItemText primary={"Orders"} />
              </ListItem>
            </Link>
            {profile.role == "customer" ? (
              ""
            ) : (
              <Link to="returns" className={styles.link}>
                <ListItem button key={"Returns"}>
                  <ListItemIcon>
                    <Forward5 />
                  </ListItemIcon>
                  <ListItemText primary={"Returns"} />
                </ListItem>
              </Link>
            )}
            {profile.role == "customer" ? (
              ""
            ) : (
              <Link to="reports" className={styles.link}>
                <ListItem button key={"Reports"}>
                  <ListItemIcon>
                    <FindInPage />
                  </ListItemIcon>
                  <ListItemText primary={"Reports"} />
                </ListItem>
              </Link>
            )}
            {profile.role == "customer" ? (
              ""
            ) : (
              <Link to="statements" className={styles.link}>
                <ListItem button key={"Statements"}>
                  <ListItemIcon>
                    <ReceiptLong />
                  </ListItemIcon>
                  <ListItemText primary={"Statements"} />
                </ListItem>
              </Link>
            )}
            {profile.role == "admin" ? (
              <Link to="promo" className={styles.link}>
                <ListItem button key={"Promo Code"}>
                  <ListItemIcon>
                    <Loyalty />
                  </ListItemIcon>
                  <ListItemText primary={"Promo Code"} />
                </ListItem>
              </Link>
            ) : (
              ""
            )}
            <Link to="logout" className={styles.link}>
              <ListItem button key={"Logout"}>
                <ListItemIcon>
                  <Logout />
                </ListItemIcon>
                <ListItemText primary={"Logout"} />
              </ListItem>
            </Link>
          </List>
          <Divider />
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <BreadCrumbs />
        <Route exact path="/home" component={Homepage} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route excat path="/profile">
          {profile.username ? <Profile /> : <Redirect to="/login" />}
        </Route>
        <Route excat path="/sales">
          {profile.username ? <Sales /> : <Redirect to="/login" />}
        </Route>
        <Route exact path="/promo">
          {profile.username ? <Promo /> : <Redirect to="/login" />}
        </Route>
        <Route exact path="/orders">
          {profile.username ? <Orders /> : <Redirect to="/login" />}
        </Route>
      </main>
    </div>
  );
}
