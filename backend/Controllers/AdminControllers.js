const { mongo } = require("mongoose");
const Nationality = require("../Models/Nationality");
const Product = require("../Models/product");
const Promo = require("../Models/promo");
const ProductOrder = require("../Models/product_order");
const Order = require("../Models/order");

exports.getOrder = async (req, res) => {
  let order = await Order.aggregate([
    {
      $lookup: {
        from: "product_orders",
        localField: "productIds",
        foreignField: "_id",
        as: "products_desc",
      },
    },
    {
      $lookup: {
        from: "products",
        localField: "products_desc.product_id",
        foreignField: "_id",
        as: "product",
      },
    },
    {
      $lookup: {
        from: "users",
        localField: "userId",
        foreignField: "_id",
        as: "user_profile",
      },
    },
    {$unwind: "$user_profile"},
    {
      $project:{
        user:{
          firstName: "$user_profile.firstName",
          lastName:  "$user_profile.lastName"
        },
        products:{
          name: "$product.name",
          quantity: "$products_desc.qty"
        },
        date: "$createdAt",
        count: {$sum:"$products_desc.qty"},
        status: "$status",
        total: "$total"
      }
    },
    {$unwind:"$products"}
  ]);

  res.json(order);
};

exports.addOrder = async (req, res) => {
  let { products, userId } = req.body;

  let total = 0;

  let addedProducts = await ProductOrder.insertMany(products);

  let productIds = [];
  addedProducts.map((element) => {
    total += element.total_price;
    productIds.push(element._id);
  });

  let order = {
    userId,
    productIds,
    total,
    status: true,
  };

  let addedOrder = await Order.create(order);

  res.json(addedOrder);
};

exports.editPromo = async (req, res) => {
  let { id } = req.params;
  let { promo } = req.body;
  try {
    const result = await Promo.updateOne(
      { _id: mongo.ObjectId(id) },
      { $set: promo }
    );
    return res.status(200).json({
      statusCode: 200,
      statusMessage: "Updated Succesfuly",
    });
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      statusCode: 500,
      statusMessage: "Internal Server Error",
    });
  }
};

exports.getPromo = async (req, res) => {
  let { id } = req.params;
  const CreatedPromo = await Promo.findById(id);
  return res.status(200).json({
    status: 200,
    data: {
      promo: CreatedPromo,
    },
  });
};

exports.StopPromo = async (req, res) => {
  let { id } = req.body;
  try {
    const result = await Promo.updateMany(
      { _id: id },
      {
        $set: {
          promoStatus: false,
        },
      }
    );
    return res.status(200).json({
      statusCode: 200,
      statusMessage: "Updated Succesfuly",
    });
  } catch (e) {
    return res.status(500).json({
      statusCode: 500,
      statusMessage: "Internal Server Error",
    });
  }
};

exports.AddPromo = async (req, res) => {
  let { promo } = req.body;
  const CreatedPromo = await Promo.create(promo);
  return res.status(200).json({
    status: 200,
    data: {
      CreatedPromo,
    },
  });
};

exports.getPromoData = async (req, res) => {
  Promise.all([Product.find({}), Promo.find({})])
    .then((results) => {
      let products = [];
      results[0].map((element) => {
        products.push({
          value: element._id,
          label: element.name,
        });
      });
      return res.status(200).json({
        status: 200,
        data: {
          products,
          promo: results[1],
        },
      });
    })
    .catch((err) => {
      throw err;
    });
};

exports.RemoveSale = async (req, res) => {
  let { saleIds } = req.body;
  try {
    const result = await Product.updateMany(
      { _id: { $in: saleIds } },
      {
        $set: {
          onSale: false,
          sale: null,
          saleFrom: null,
          saleTo: null,
        },
      },
      { multi: true }
    );
    return res.status(200).json({
      statusCode: 200,
      statusMessage: "Updated Succesfuly",
    });
  } catch (e) {
    return res.status(500).json({
      statusCode: 500,
      statusMessage: "Internal Server Error",
    });
  }
};

exports.AddSale = async (req, res) => {
  let { saleIds, data } = req.body;
  try {
    const result = await Product.updateMany(
      { _id: { $in: saleIds } },
      {
        $set: {
          onSale: true,
          sale: data.saleAmount,
          saleFrom: data.saleFrom,
          saleTo: data.saleTo,
        },
      },
      { multi: true }
    );
    return res.status(200).json({
      statusCode: 200,
      statusMessage: "Updated Succesfuly",
    });
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      statusCode: 500,
      statusMessage: "Internal Server Error",
    });
  }
};

exports.Search = async (req, res) => {
  const { search } = req.body;

  const qGroup = search.group ? { group: search.group } : {};
  const qGender = search.gender ? { gender: search.gender } : {};
  const qonSale =
    search.onSale == false || search.onSale == true
      ? { onSale: search.onSale }
      : {};
  const qDate =
    search.toDate && search.fromDate
      ? {
          date: {
            $gte: ISODate(new Date(search.fromDate)),
            $lte: ISODate(new Date(search.toDate)),
          },
        }
      : {};

  const returnedProducts = await Product.aggregate([
    { $match: { $and: [qGroup, qGender, qonSale, qDate] } },
  ]);

  return res.status(200).json(returnedProducts);
};

exports.SearchGet = async (req, res) => {
  const products = await Product.find({});
  return res.status(200).json({
    status: 200,
    data: {
      products,
    },
  });
};

exports.addProduct = async (req, res) => {
  // const {product} = req.body
  // product.forEach(async element => {
  //     element.date = new Date(element.date)
  //     const createdProduct = await Product.create(element)
  // });
  // res.status(200).json({status:"added"})
};

exports.getData = async (req, res) => {
  const nationality = await Nationality.find({});
  return res.status(200).json({
    status: 200,
    data: {
      nationality,
    },
  });
};
