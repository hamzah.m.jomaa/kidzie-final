const express = require("express");
const router = express.Router();
const AdminController = require("../Controllers/AdminControllers")


router.post("/edit/:id",AdminController.editPromo)
router.get("/code/:id",AdminController.getPromo)
router.post("/stop",AdminController.StopPromo)
router.post("/add",AdminController.AddPromo)
router.get("/",AdminController.getPromoData)


module.exports = router