const Auth = require("./Auth")
const getRequests = require("./getRequests")
const Profile = require("./profile")
const Products = require("./products")
const Promo = require("./promo")
const Orders = require("./orders")


module.exports.Router = (app) =>{
    app.use("/api/",getRequests)
    app.use("/api/orders",Orders)
    app.use("/api/products",Products)
    app.use("/api/promo",Promo)
    app.use("/api/profile",Profile)
    app.use("/api/user",Auth)
}