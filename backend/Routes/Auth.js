const express = require("express");
const router = express.Router();
const AuthUser = require("../Controllers/AuthUser")
const AdminController = require("../Controllers/AdminControllers")

router.post("/register",AuthUser.Register)
router.post("/login",AuthUser.Login)

module.exports = router