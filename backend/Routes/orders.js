const express = require("express");
const router = express.Router();
const AdminController = require("../Controllers/AdminControllers")


router.post("/add",AdminController.addOrder)
router.get("/",AdminController.getOrder)

module.exports = router