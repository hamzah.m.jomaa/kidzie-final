const { Schema, model } = require("mongoose")
const product_order = require("./product_order")
const user = require("./User")

const schema = Schema({
    userId:{
        type: Schema.Types.ObjectId,
        ref: user
    },
    productIds:[{
        type: Schema.Types.ObjectId,
        ref: product_order
    }],
    total:{
        type: Number
    },
    status:{
        type: Boolean
    }
},{timestamps:true})
module.exports = model("orders",schema)
