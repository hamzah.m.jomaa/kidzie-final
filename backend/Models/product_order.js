const { Schema, model } = require("mongoose")
const product = require("./product")

const schema = Schema({
    product_id:{
        type: Schema.Types.ObjectId,
        ref: product
    },
    qty:{
        type: Number,
        required: [true,"Quantity of Product is Required"]
    },
    item_price:{
        type: Number,
        required: [true,"Item Price is Required"]
    },
    total_price:{
        type: Number,
        required: [true,"Item Price is Required"]
    }
},{timestamps:true})
module.exports = model("product_order",schema)
