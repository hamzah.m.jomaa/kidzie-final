const { Schema, model } = require("mongoose")
const Product = require("./product")

const schema = Schema({
    promoStatus:{
        type:Boolean,
        required: [true,"Promo Status is Required"]

    },
    productId:[{
        type:Schema.Types.ObjectId,
        ref: Product
    }],
    promoPercent:{
        type:Number,
    },
    promoCode:{
        type:String
    },
    fromDate:{
        type:Date
    },
    toDate:{
        type:Date
    },
    email:[{
        type:String
    }],
    promoCiteria:{
        categories:[{
            type:String
        }],
        designer:{
            type:Number
        },
        productGroup:{
            type:Number
        },
    }
},{timestamps:true})

module.exports = model("promo",schema)


