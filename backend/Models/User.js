const { Schema, model } = require("mongoose")

const schema = Schema({
    firstName:{
        type:String,
        required: [true,"FirstName is Required"]
    },
    lastName:{
        type:String,
        required: [true,"LastName is Required"]
    },
    email:{
        type:String,
        required: [true,"Email is Required"]
    },
    phoneNumber:{
        type:String,
        required: [true,"PhoneNumber is Required"]
    },
    country:{
        type:String,
        required: [true,"Country is Required"]
    },
    gender:{
        type:String,
        require: [true,"Gender is Required"]
    },
    username:{
        type:String,
        require: [true,"Username is Required"]
    },
    password:{
        type:String,
        require: [true,"Password is Required"]
    },
    role:{
        type:String,
        require: [true,"User Role is required"]
    }
},    {timestamps:true})

module.exports = model("Users",schema)
