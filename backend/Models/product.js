const { Schema, model } = require("mongoose")

const schema = Schema({
    name:{
        type:String,
        required: [true,"Product Name is Required"]
    },
    description:{
        type:String,
        required: [true,"Product Description is Required"]
    },
    date:{
        type:Date,
        required: [true,"Product Date is Required"]
    },
    group:{
        type: Number,
    },
    onSale:{
        type:Boolean,
    },
    sale:{
        type:Number,
    },
    age:{
        type:Number
    },
    gender:{
        type:String
    },
    designer:{
        type:Number
    },
    price:{
        type:Number
    },
    category:{
        type:String
    },
    saleFrom:{
        type:Date
    },
    saleTo:{
        type:Date
    }
},{timestamps:true})

module.exports = model("products",schema)